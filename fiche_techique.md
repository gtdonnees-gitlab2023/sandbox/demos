

à partir de la source [Wikipédia](https://fr.wikipedia.org/wiki/Le_Bon,_la_Brute_et_le_Truand), on trouve (copié - collé) :

-  Titre français et québécois : Le Bon, la Brute et le Truand (Titre français original d'exploitation: Le bon la brute le truand)
-  Titre original italien : Il buono, il brutto, il cattivo
-  Titre espagnol : El bueno, el feo y el malo
-  Titre allemand : Zwei glorreiche Halunken
-  Réalisation : Sergio Leone
-  Scénario : Agenore Incrocci, Furio Scarpelli, Luciano Vincenzoni et Sergio Leone, d'après une histoire de Luciano Vincenzoni et Sergio Leone
-  Musique : Ennio Morricone
-  Décors et costumes : Carlo Simi
-  Photographie : Tonino Delli Colli
-  Son : Elio Pacella et Vittorio De Sisti
-  Montage : Eugenio Alabiso et Nino Baragli
-  Production : Alberto Grimaldi
-  Sociétés de production : Produzioni Europee Associati, Arturo González Producciones Cinematográficas3 et Constantin Film Produktion3
-  Sociétés de distribution : Produzioni Europee Associati (Italie), United Artists (États-Unis), Les Artistes Associés (France)
-  Budget : 1 200 000 dollars US4
-  Pays de production : Drapeau de l'Italie Italie, Drapeau de l'Espagne Espagne et Allemagne de l'Ouest Allemagne de l'Ouest
-  Langue originale : italien
-  Format : couleur (Technicolor) — 35 mm — 2,33:1 (Techniscope) — son mono
-  Genre : western spaghetti
-  Durée : 161 minutes (178 minutes pour la version longue de 2002)
-  Dates de sortie :
-  -  Italie : 23 décembre 1966
-  -  Allemagne de l'Ouest : 15 décembre 1967
-  -  France : 8 mars 1968
-  -  Espagne : 7 août 1968
