# Projet **demos**

Journée Gitlab 29/06

Démonstration de l'utilisation de Gitlab

## But de ce projet GitLab

L'objectif de ce projet est d'illustrer les différentes activités qui peuvent avoir lieu dans un projet Gitlab à travers la création et l'évolution d'un ensemble de fichiers décrivant le film *Le bon, La brute et le truand* de **Sergio Leone**. 

## Contenu du dépôt

Dans l'idéal, la production du film a besoin : 

1. Du synopsis
2. De la liste des acteurs principaux
3. De la fiche technique comportant : 
    * Nom du réalisateur
    * Date de sortie
    * Durée

